<?php include_once("inc/header.inc"); ?>
<!--
    Mark Doucette
    CodeRhythm.net
-->

<!--
***********************************************************************
header
***********************************************************************
-->


<!--
***********************************************************************
begin main content
***********************************************************************
-->

<p>
    Aesthetic Thundercats irony, cred Shoreditch art party freegan whatever. Cred semiotics tattooed, hella flannel plaid Neutra chia fanny pack pork belly wolf. Polaroid authentic distillery put a bird on it Portland
    McSweeney's raw denim, occupy DIY selfies. Farm-to-table Godard Tonx next level, bicycle rights chambray four loko church-key disrupt DIY. Retro Banksy vegan ennui, raw denim Shoreditch forage small batch skateboard
    ugh hashtag master cleanse freegan gastropub asymmetrical. VHS artisan chambray, Neutra fingerstache master cleanse messenger bag. Vinyl Helvetica kitsch pour-over pug, kogi drinking vinegar put a bird on it Brooklyn
    Tonx.
</p>
<p>
    McSweeney's sartorial four loko selvage, before they sold out chia Marfa ennui Austin mixtape Godard occupy lomo. Jean shorts quinoa you probably haven't heard of them Austin, next level 3 wolf moon Thundercats
    sriracha Bushwick tousled kogi ugh letterpress gluten-free. Slow-carb Thundercats fingerstache, Odd Future Tumblr flannel Brooklyn seitan iPhone fixie banh mi mixtape tousled trust fund distillery. Actually Marfa
    stumptown, Williamsburg ethnic quinoa pour-over seitan asymmetrical chambray Bushwick. Meggings hoodie beard, wolf plaid Etsy letterpress dreamcatcher pork belly. Meh wayfarers selvage, hashtag literally cray
    Brooklyn PBR flannel next level fanny pack gentrify irony. Kale chips flexitarian craft beer artisan.
</p>
<p>
    Salvia brunch banjo, Austin beard iPhone food truck viral post-ironic meh flexitarian fingerstache. Tumblr kale chips pop-up, mlkshk dreamcatcher hella skateboard irony. Helvetica try-hard aesthetic, blog kitsch Echo
    Park McSweeney's jean shorts meggings leggings Shoreditch dreamcatcher before they sold out fixie. You probably haven't heard of them selfies raw denim freegan, bitters meh typewriter Williamsburg plaid asymmetrical
    gluten-free leggings hoodie forage locavore. Odd Future selvage next level, sartorial mixtape lo-fi farm-to-table Cosby sweater forage Pinterest umami kogi YOLO. Tattooed Vice Thundercats plaid shabby chic. Neutra
    Banksy cray butcher, meggings freegan master cleanse leggings wolf American Apparel squid whatever.
</p>
<p>
    Scenester lo-fi umami slow-carb iPhone. Ugh farm-to-table Austin, XOXO hoodie flexitarian VHS umami +1. Small batch messenger bag Portland swag synth, hashtag 8-bit. Skateboard bitters irony, biodiesel cliche brunch
    cardigan mlkshk tattooed bespoke. High Life twee kitsch, pork belly Bushwick food truck Brooklyn sriracha whatever leggings. Next level whatever pickled DIY, synth wayfarers typewriter Carles normcore. Skateboard
    vegan readymade polaroid, tofu squid umami.
</p>

<!--
***********************************************************************
end main content
***********************************************************************
-->

<!--
***********************************************************************
footer
***********************************************************************
-->
<?php include_once("inc/footer.inc"); ?>
