<?php include_once("inc/header.inc"); ?>
<!--
    Mark Doucette
    CodeRhythm.net
-->

<!--
***********************************************************************
header
***********************************************************************
-->


<!--
***********************************************************************
begin main content
***********************************************************************
-->

<article class="main_article">
    <header class="header_form"><h2>Site Info</h2></header>
    <div id="sitemap">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Resources</a>
                <ul class="sub_menu">
                    <li><a href="video.php">Video</a></li>
                    <li><a href="books.php">Books</a></li>
                </ul>
            </li>
            <li><a href="about.php">About Me</a></li>
            <li><a href="portfolio.php">Portfolio</a></li>
            <li><a href="contact.php">Contact</a></li>
        </ul>
    </div>
</article>

<!--
***********************************************************************
end main content
***********************************************************************
-->

<!--
***********************************************************************
footer
***********************************************************************
-->
<?php include_once("inc/footer.inc"); ?>
