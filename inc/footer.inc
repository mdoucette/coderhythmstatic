</div> <!-- end id="main_content" -->
<footer>
    <div id="secondary_nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="portfolio.php">Portfolio</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="sitemap.php">Sitemap</a></li>

        </ul>
    </div>
    <div id="copyright">
        <p>Copyright &copy;2014 Rhythm Technology</p>
    </div>
    <div id="social">
        <h4>Connect</h4>
        <ul>
            <li><a href="https://twitter.com/DoucetteMark" class="twitter">twitter</a></li>
            <li><a href="http://www.linkedin.com/pub/mark-doucette/9a/186/504" class="linkedin">linkedin</a></li>
        </ul>
    </div>
</footer>
</div> <!-- end id="container" -->
</body>
</html>