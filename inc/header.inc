<!doctype html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Code Rhythm</title>
    <!-- Reset stylesheet from HTML5Doctor.com -->
    <link href="css/reset.css" type="text/css" rel="stylesheet"/>
    <link href="css/default.css" type="text/css" rel="stylesheet"/>
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container">
    <div class="search_bar">
        <form>
            <!-- Use JavaScript here to make the word 'Search' disappear on when user clicks the input box -->
            <span><input type="text" class="search" value="Search" tabindex="501" onfocus=" this.value = ''; " onblur="if (this.value == '') { this.value='Search';} "></span>
        </form>
    </div>
    <header>
        <div class="hgroup">
            <h1>Code::<span>Rhythm</span></h1>

            <h2>thoughts.on( <span>programming</span> )</h2>
        </div>
        <nav>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Resources</a>
                    <ul class="sub_menu">
                        <li><a href="video.php">Video</a></li>
                        <li><a href="books.php">Books</a></li>
                    </ul>
                </li>
                <li><a href="about.php">About Me</a></li>
                <li><a href="portfolio.php">Portfolio</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
    </header>
    <div id="main_content">






